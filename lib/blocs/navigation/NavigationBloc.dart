import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nkhape/blocs/navigation/events/NavigationEvents.dart';
import 'package:nkhape/blocs/navigation/states/NavigationStates.dart';

class NavigationBloc extends Bloc<NavigationEvent, dynamic> {
  NavigationBloc() : super(InitState());

  @override
  Stream<dynamic> mapEventToState(NavigationEvent event) async* {
    
    if (event is NavigateToLogin) {
      yield 1;
      
    } else if (event is NavigateToMenu) {
      yield 2;
      
    } else if (event is NavigateToTravel) {
      print("going to travel");
      yield 3;
      
    } else if (event is NavigateToTrips) {
      yield 4;
      
    } else if (event is NavigateToProfile) {
      yield 5;
      
    } else {
       yield 0;
    }
  }

}