abstract class NavigationEvent {}

class NavigateToLogin extends NavigationEvent {}

class NavigateToMenu extends NavigationEvent {}

class NavigateToTravel extends NavigationEvent {}

class NavigateToProfile extends NavigationEvent {}

class NavigateToTrips extends NavigationEvent {}