abstract class MenuEvent {}

class SelectMenuItemEvent extends MenuEvent {

  final String field;

  SelectMenuItemEvent({required this.field});
}