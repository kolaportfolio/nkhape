import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nkhape/blocs/menu/events/MenuEvents.dart';
import 'package:nkhape/blocs/menu/states/MenuStates.dart';

class MenuBloc extends Bloc<MenuEvent, MenuState> {
  MenuBloc() : super(InitMenuState());

  @override
  Stream<MenuState> mapEventToState(MenuEvent event) async* {
    if (event is SelectMenuItemEvent) {
      yield MenuSelectedState(menuItemName: event.field);
    }
  }
}