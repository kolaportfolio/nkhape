import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nkhape/blocs/menu/states/MenuStates.dart';
import 'package:nkhape/screens/travel.dart';

import '../MenuBloc.dart';

class MenuListener extends StatelessWidget {
  const MenuListener({super.key});

  @override
  Widget build(BuildContext context) =>
      BlocBuilder<MenuBloc, MenuState>(
        builder: (context, state) {
          if (state is MenuSelectedState) {
            if (state.menuItemName == 'travel') {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => const TravelPage()
                  )
              );
            }
          }
          return Container();
        },
      );
}