abstract class MenuState {}

class MenuSelectedState extends MenuState{
  final String menuItemName;

  MenuSelectedState({required this.menuItemName});
}

class InitMenuState extends MenuState {}