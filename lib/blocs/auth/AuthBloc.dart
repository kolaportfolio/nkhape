import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nkhape/blocs/auth/events/AuthEvents.dart';
import 'package:nkhape/blocs/auth/states/AuthStates.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  AuthBloc() : super(InitAuthState());

  @override
  Stream<AuthState> mapEventToState(AuthEvent event) async* {
    if (event is LoginEvent) {
      print("lOGIN EVENT");
      yield SuccessAuthState(username: event.username);
    }
  }
}