import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nkhape/blocs/auth/states/AuthStates.dart';
import 'package:nkhape/screens/menu.dart';

import '../AuthBloc.dart';

class AuthListener extends StatelessWidget {
  const AuthListener({super.key});

  @override
  Widget build(BuildContext context) =>
    BlocBuilder<AuthBloc, AuthState>(
      builder: (context, state) {
        if (state is SuccessAuthState) {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => const Menu()
              )
          );
        }
        return Container();
      },
    );

}