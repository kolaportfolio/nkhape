abstract class AuthState {}

class InitAuthState extends AuthState {}

class SuccessAuthState extends AuthState {
  final String username;

  SuccessAuthState({required this.username});
}

class ErrorAuthState extends AuthState {
  final String error;

  ErrorAuthState({required this.error});
}