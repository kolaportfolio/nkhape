import 'package:flutter/material.dart';

class NkhapeGUIConstants {
  late final MaterialColor mainRed;
  late final MaterialColor mainBlack;
  late final MaterialColor mainWhite;

  static const Color mRed = Color(0xFFFF6F59);
  static const Color bgBlack = Color(0xFF343434);
  static const Color mWhite = Color(0xFFD9D9D9);

  late final TextStyle buttonTextStyle;
  late final TextStyle mainLogo;
  late final TextStyle headerTextStyle;
  late final TextStyle labelTextStyle;

  NkhapeGUIConstants()  {
    mainRed = generateColor(const Color.fromRGBO(255, 111, 89, 1));
    mainBlack = generateColor(const Color.fromRGBO(57, 57, 57, 1));
    mainWhite = generateColor(const Color.fromRGBO(217, 217, 217, 1));

    buttonTextStyle = const TextStyle(
      fontFamily: 'JosefinSans',
      fontWeight: FontWeight.bold,
      fontSize: 20.0,
      color: Color(0xFFD9D9D9),
      shadows: [
        Shadow(
          color: bgBlack,
          offset: Offset(2, 2),
          blurRadius: 2
        )
      ]
    );

    mainLogo = const TextStyle(
      fontFamily: 'JosefinSans',
      fontWeight: FontWeight.bold,
      fontSize: 30.0,
      color: Color(0xFFFFFFFF)
    );

    headerTextStyle = const TextStyle(
      fontFamily: 'JosefinSans',
      fontWeight: FontWeight.bold,
      fontSize: 25.0,
      color: Color(0xFFFFFFFF)
    );

    labelTextStyle = const TextStyle(
        fontFamily: 'JosefinSans',
        fontWeight: FontWeight.bold,
        fontSize: 15.0,
        color: Color(0xFFD9D9D9)
    );
  }

  Size textEditSize = const Size(207, 50);
  Size wideButtonSize = const Size(280, 50);
  Size roundButtonSize = const Size(100, 100);
  BorderRadius buttonRadius = BorderRadius.circular(20.0);
  
  static MaterialColor generateColor(Color color) {
    List<Color> shades = [];
    const int numOfShades = 5;
    
    for (int count = 1; count <= numOfShades; count++){
      double strength = count / numOfShades;
      
      int red = ((1 - strength) * color.red).round();
      int green = ((1 - strength) * color.green).round();
      int blue = ((1 - strength) * color.blue).round();
      
      shades.add(Color.fromRGBO(red, green, blue, 1));
    }

    Map<int, Color> swatch = { for (var item in
      List.generate(numOfShades, (index) => (index + 1) * 100))
      item : shades[item ~/ 100 - 1]
    };

    return MaterialColor(color.value, swatch);
  }

}