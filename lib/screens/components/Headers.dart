import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nkhape/utils/constants.dart';

class Headers extends StatelessWidget {
  final NkhapeGUIConstants myStyle = NkhapeGUIConstants();
  final String child;

  Headers({
    super.key,
    required this.child
  });

  @override
  Widget build(BuildContext context) {

    final textPainter = TextPainter(
      text:TextSpan(
        text: child,
        style: myStyle.headerTextStyle
      ),
      textDirection: TextDirection.ltr
    )..layout();

    final width = textPainter.width;

    return Container(
        padding: const EdgeInsets.only(
            bottom: 50.0
        ),
        child: Column(
            children: [
              Text(
                child,
                style: myStyle.headerTextStyle,
              ),
              Row(
                children: [
                  Container(
                    width: width + 100.0,
                    height: 2.0,
                    color: NkhapeGUIConstants.mRed,
                  ),
                  const CircleAvatar(
                    backgroundColor: NkhapeGUIConstants.mRed,
                    radius: 4.0,
                  )
                ],
              )
            ]
        )
    );
  }
}