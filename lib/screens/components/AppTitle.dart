import 'package:flutter/cupertino.dart';
import 'package:nkhape/utils/constants.dart';

class AppTitle extends StatelessWidget{

  final NkhapeGUIConstants constants = NkhapeGUIConstants();

  AppTitle({super.key});

  @override
  Widget build(BuildContext context) =>
      Text(
        'NKHAPE',
        style: constants.mainLogo,
      );
}