import 'package:flutter/material.dart';
import 'package:nkhape/utils/constants.dart';

class RoundButton extends StatelessWidget {
  final NkhapeGUIConstants myStyle = NkhapeGUIConstants();
  final VoidCallback onPressed;
  final String imagePath;
  final String child;

  RoundButton({
    super.key,
    required this.onPressed,
    required this.child,
    required this.imagePath,
  });

  @override
  Widget build(BuildContext context) =>
      Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          ElevatedButton(
            style: ElevatedButton.styleFrom(
              elevation: 5,
              shape: const CircleBorder(),
              backgroundColor: myStyle.mainRed,
              fixedSize: myStyle.roundButtonSize,
            ),
            onPressed: onPressed,
            child: DefaultTextStyle(
              style: myStyle.buttonTextStyle,
              child: Image.asset(
                imagePath,
              ),
            ),
          ),
          Padding(
              padding: const EdgeInsets.only(
                top: 20.0,
              ),
            child: Text(
                child,
              style: myStyle.labelTextStyle,
            ),
          )

        ],
      );
}