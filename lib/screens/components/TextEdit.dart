import 'package:flutter/material.dart';
import 'package:nkhape/utils/constants.dart';

class TextEdit extends StatelessWidget{

  final NkhapeGUIConstants constants = NkhapeGUIConstants();
  final String labelText;
  final String field;
  final TextEditingController controller;
  final bool hide;

  TextEdit({
    super.key,
    required this.labelText,
    required this.field,
    required this.controller,
    required this.hide
  });


  @override
  Widget build(BuildContext context) => SizedBox(
    width: constants.textEditSize.width,
    child: TextField(
      style: constants.buttonTextStyle,
      controller: controller,
      obscureText: hide,
      cursorColor: NkhapeGUIConstants.mRed,
      decoration: InputDecoration(
        labelText: labelText,
        labelStyle: constants.buttonTextStyle,
        contentPadding: const EdgeInsets.only(
            top: 4.0
        ),
        enabledBorder: const UnderlineInputBorder(
            borderSide: BorderSide(
              color: NkhapeGUIConstants.mWhite,
            )
        ),
        focusedBorder: const UnderlineInputBorder(
            borderSide: BorderSide(
                color: NkhapeGUIConstants.mRed
            )
        ),
      ),
    ),
  );

}