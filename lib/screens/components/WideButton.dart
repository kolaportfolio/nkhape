import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nkhape/utils/constants.dart';

class WideButton extends StatelessWidget{
  final NkhapeGUIConstants myStyle = NkhapeGUIConstants();
  final VoidCallback onPressed;
  final Widget child;

  WideButton({
    super.key,
    required this.onPressed,
    required this.child,
  });

  @override
  Widget build(BuildContext context) =>
      ElevatedButton(
        style: ElevatedButton.styleFrom(
          elevation: 5,
          shape: RoundedRectangleBorder(
            borderRadius: myStyle.buttonRadius
          ),
          backgroundColor: myStyle.mainRed,
          fixedSize: myStyle.wideButtonSize,
        ),
          onPressed: onPressed,
          child: DefaultTextStyle(
            style: myStyle.buttonTextStyle,
            child: child,
          ),
      );
}