import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nkhape/blocs/navigation/NavigationBloc.dart';
import 'package:nkhape/utils/constants.dart';

class TravelPage extends StatelessWidget {
  const TravelPage({super.key});

  @override
  Widget build(BuildContext context){
    return BlocListener<NavigationBloc, dynamic>(
        listener: (context, state) {

        },
        child: BlocBuilder<NavigationBloc, dynamic>(
          builder: (context, state) {
            return const TravelScreen();
          },
        ),
    );
  }

}

class TravelScreen  extends StatelessWidget{
  const TravelScreen({super.key});


  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      backgroundColor: NkhapeGUIConstants.bgBlack,
      body: Center(
        child: Text('vibes'),
      ),
    );
  }
}