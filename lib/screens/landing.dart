import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nkhape/blocs/auth/AuthBloc.dart';
import 'package:nkhape/blocs/auth/events/AuthEvents.dart';
import 'package:nkhape/blocs/auth/listeners/AuthListener.dart';
import 'package:nkhape/blocs/navigation/NavigationBloc.dart';
import 'package:nkhape/screens/components/AppTitle.dart';
import 'package:nkhape/screens/components/TextEdit.dart';
import 'package:nkhape/screens/components/WideButton.dart';
import 'package:nkhape/screens/menu.dart';
import 'package:nkhape/utils/constants.dart';

class LandingPage extends StatelessWidget{

  const LandingPage({super.key});

  @override
  Widget build(BuildContext context) {

    return  BlocListener<NavigationBloc, dynamic>(
      listener: (context, state) {
        if (state != null && state == 2) {
          Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                  builder: (context) => const Menu()
              )
          );
        }
      },
      child: Scaffold(
        body: Center(
          child: BlocBuilder<NavigationBloc, dynamic>(
            builder: (context, state) {
                return LoginScreen();
            },
          ),
        ),
      ),

    );
  }

}

class LoginScreen extends StatelessWidget{
  LoginScreen({super.key});

  TextEditingController  usernameController = TextEditingController();
  TextEditingController  passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: NkhapeGUIConstants.bgBlack,
        body:Center(
            child: SizedBox(
                height: MediaQuery.of(context).size.height,
                child: Column(
                  children: [
                    Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(
                              top: 100.0,
                              bottom: 80.0
                          ),
                          child: AppTitle(),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                              top: 25.0,
                              bottom: 65.0
                          ),
                          child: Image.asset('assets/NkhapeLogo.png'),
                        )
                      ],
                    ),
                    Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(
                              top: 20.0,
                              bottom: 20.0
                          ),
                          child: TextEdit(
                            labelText: "username",
                            field: "username",
                            controller: usernameController, hide: false,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                              top: 20.0,
                              bottom: 20.0
                          ),
                          child: TextEdit(
                            labelText: "password",
                            field: "password",
                            controller: passwordController, hide: true,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                              top:20.0,
                              bottom: 20.0
                          ),
                          child: WideButton(
                            onPressed: () {
                              AuthBloc authBloc = BlocProvider.of<AuthBloc>(context);
                              authBloc.add(
                                  LoginEvent(
                                      username: usernameController.text,
                                      password: passwordController.text
                                  )
                              );
                            },
                            child: const Text('Login'),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                              top:20.0,
                              bottom: 20.0
                          ),
                          child: WideButton(
                            onPressed: () => {

                            },
                            child: const Text('Sign Up'),
                          ),
                        ),
                        const AuthListener()
                      ],
                    )
                  ],
                )
            )
        )
    );
  }

}