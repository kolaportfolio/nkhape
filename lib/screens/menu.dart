import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nkhape/blocs/navigation/NavigationBloc.dart';
import 'package:nkhape/blocs/navigation/events/NavigationEvents.dart';
import 'package:nkhape/screens/components/AppTitle.dart';
import 'package:nkhape/screens/components/Headers.dart';
import 'package:nkhape/screens/components/RoundButton.dart';
import 'package:nkhape/screens/landing.dart';
import 'package:nkhape/screens/travel.dart';
import 'package:nkhape/utils/constants.dart';

class Menu extends StatelessWidget{

  const Menu({super.key});


  @override
  Widget build(BuildContext context) {
    return BlocListener<NavigationBloc, dynamic>(
        listener: (context, state) {
          print(state);
          if (state == 3) {
            print("going to travel haha");
            Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                    builder: (context) => const TravelPage()
                )
            );
          }
        },
      child: BlocBuilder<NavigationBloc, dynamic>(
        builder: (context, state) {
          print("really");
          if (state == 3) {
            print("going to travel haha");
          }
          return const Scaffold(
            body: Center(
              child: MenuScreen(),
            ),
          );
        },)
    );
  }

}

class MenuScreen extends StatelessWidget{
  const MenuScreen({super.key});


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: NkhapeGUIConstants.bgBlack,
      appBar: AppBar(
          elevation: 0,
          shadowColor: Colors.transparent,
          backgroundColor: NkhapeGUIConstants.bgBlack,
          leading: Padding(
            padding: const EdgeInsets.all(2.0),
            child: GestureDetector(
              child: Image.asset('assets/BackArrow.png'),
            ),
          )
      ),
      body: Column(
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.only(
                    top: 20.0,
                    bottom: 30.0
                ),
                child: AppTitle(),
              ),
            ],
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [

              Padding(
                padding: const EdgeInsets.only(
                    bottom: 30.0
                ),
                child: Headers(
                    child: 'Menu'
                ),
              )

            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.only(
                    bottom: 50
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    RoundButton(
                        onPressed: () {
                          context.read<NavigationBloc>().add(NavigateToTravel());
                          print("special");
                          Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const TravelPage()
                              )
                          );
                        },
                        imagePath: 'assets/travelIcon.png',
                        child: 'Travel'
                    ),
                    RoundButton(
                        onPressed: () => {},
                        imagePath: 'assets/tripsIcon.png',
                        child: 'Trips'
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    bottom: 50
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    RoundButton(
                        onPressed: () => {},
                        imagePath: 'assets/profileIcon.png',
                        child: 'Profile'
                    ),
                    RoundButton(
                        onPressed: () => {},
                        imagePath: 'assets/searchIcon.png',
                        child: 'Search'
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    bottom: 0
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    RoundButton(
                        onPressed: () => {},
                        imagePath: 'assets/reportIcon.png',
                        child: 'Report'
                    )
                  ],
                ),
              )
            ],
          ),
        ],
      ),
    );
  }

}